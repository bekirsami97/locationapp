package com.marti.proje.mainFragment

import android.util.Log
import com.marti.proje.install.ObservableViewModel
import androidx.databinding.Bindable
import androidx.databinding.library.baseAdapters.BR
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.marti.proje.model.Place
import com.marti.proje.repository.Repository
import com.marti.proje.retrofit.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainFragmentViewModel(val repository: Repository): ObservableViewModel() {

    val data:MutableLiveData<Resource<Place>> = MutableLiveData()


     fun getPlace() = viewModelScope.launch(Dispatchers.IO) {
              data.postValue(Resource.loading(data = null))
              try {
                 data.postValue(Resource.success(data = repository.getPlace(_displayedEdittext.value)))
              } catch (exception: Exception) {
                  data.postValue(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
              }
          }

    @Bindable
    val editText = MutableLiveData<String>()

    val _displayedEdittext = MutableLiveData<String>()

    val displayedEdittext:LiveData<String>
        get() = _displayedEdittext


    fun setRememberMe(value: String) {
        if (displayedEdittext.value != value) {
            _displayedEdittext.value = value

            notifyPropertyChanged(BR.editText)
        }
    }





}