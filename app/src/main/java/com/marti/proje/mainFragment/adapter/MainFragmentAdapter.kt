package com.marti.proje.mainFragment.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.library.baseAdapters.BR
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.marti.proje.R
import com.marti.proje.databinding.BindingMainItems
import com.marti.proje.model.Result

class MainFragmentAdapter(val event:MainFragmentClickListener):ListAdapter<Result,MainFragmentAdapter.MyViewHolder>(DiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.search_items_layout,parent,false))
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.initView(getItem(position),event)
    }

    inner class MyViewHolder(val bind: BindingMainItems): RecyclerView.ViewHolder(bind.root){
        fun initView(item: Result,listener: MainFragmentClickListener) {
            bind.setVariable(BR.list,item)
            bind.executePendingBindings()
            bind.container.setOnClickListener {
                listener.onClick(item)
            }
        }
    }

    class DiffCallback: DiffUtil.ItemCallback<Result>(){
        override fun areItemsTheSame(oldItem: Result, newItem: Result): Boolean {
            return oldItem.formatted_address == newItem.formatted_address
        }

        override fun areContentsTheSame(oldItem: Result, newItem: Result): Boolean {
            return oldItem == newItem
        }

    }

}