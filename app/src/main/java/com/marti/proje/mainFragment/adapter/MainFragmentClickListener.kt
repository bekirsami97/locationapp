package com.marti.proje.mainFragment.adapter

import com.marti.proje.model.Result

interface MainFragmentClickListener {
    fun onClick(model:Result)

}
