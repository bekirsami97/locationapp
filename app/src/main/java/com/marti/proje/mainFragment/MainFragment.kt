package com.marti.proje.mainFragment

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.observe
import androidx.navigation.findNavController
import com.marti.proje.R

import com.marti.proje.databinding.BindingMainFragment
import com.marti.proje.mainFragment.adapter.MainFragmentAdapter
import com.marti.proje.mainFragment.adapter.MainFragmentClickListener
import com.marti.proje.mapActivity.MapActivity
import com.marti.proje.model.Result
import com.marti.proje.retrofit.Resource
import org.koin.android.viewmodel.ext.android.viewModel
import retrofit2.Response

/**
 * A simple [Fragment] subclass.
 */
class MainFragment : Fragment() {

    lateinit var mBind:BindingMainFragment
    val mViewModel:MainFragmentViewModel by viewModel()
     var mAdapter:MainFragmentAdapter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mBind =  BindingMainFragment.inflate(inflater, container, false)
         mBind.lifecycleOwner = this
        mBind.viewModel = mViewModel
        mBind.isLoad = false
        return mBind.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //Keep edittext's text as liveData
        mViewModel.editText.observe(viewLifecycleOwner, Observer {
            mViewModel.setRememberMe(it)
        })

        //keyboard search action
        mBind.searchEdittext.setOnEditorActionListener { textView, i, keyEvent ->
            if (i == EditorInfo.IME_ACTION_SEARCH){
                mViewModel.getPlace()
                return@setOnEditorActionListener true
            }
            return@setOnEditorActionListener  false
        }

        mBind.buttonSearch.setOnClickListener {
            mViewModel.getPlace()
        }

        mAdapter = MainFragmentAdapter(object : MainFragmentClickListener{
            override fun onClick(model: Result) {
                val intent = Intent(context,MapActivity::class.java)
                intent.putExtra("dataModel",model)
                startActivity(intent)
            }
        })

        //Get Searh Data
        mViewModel.data.observe(viewLifecycleOwner, Observer {
            it?.let {
                when(it.status){
                    Resource.Status.SUCCESS ->{
                        it.data?.error_message?.let {message ->
                            Toast.makeText(context,message,Toast.LENGTH_SHORT).show()
                        }
                        mAdapter?.submitList(it.data?.results)
                        setRecylerView()
                    }
                    Resource.Status.ERROR ->{
                        Toast.makeText(context,it.message.toString(),Toast.LENGTH_SHORT).show()
                        mBind.isLoad = false
                    }
                    Resource.Status.LOADING ->{
                        mBind.isLoad = true
                    }
                }
            }
        })
    }


    fun setRecylerView(){
        mBind.recylerView.apply {
            adapter = mAdapter
        }
        mBind.isLoad = false
    }

}
