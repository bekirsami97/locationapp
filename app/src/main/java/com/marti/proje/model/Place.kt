package com.marti.proje.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Place(@SerializedName("error_message") val error_message:String,
                   @SerializedName("results") val results:MutableList<Result>
):Serializable

data class Result(
    @SerializedName("formatted_address") val formatted_address:String?,
    @SerializedName("geometry") val geometry:Geometry?,
    @SerializedName("photos") val photos:MutableList<Photo>?,
    @SerializedName("id") val id:String?,
    @SerializedName("name") val name:String?

    ):Serializable

data class Geometry(
    @SerializedName("location") val location:Location?

):Serializable

data class Location(
    @SerializedName("lat") val lat:Double?,
    @SerializedName("lng") val lng:Double?
):Serializable


data class Photo(
    @SerializedName("height") val height:Int,
    @SerializedName("photo_reference") val photo_reference:String,
    @SerializedName("width") val width:Int
):Serializable