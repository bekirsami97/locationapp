package com.marti.proje.repository

import android.annotation.SuppressLint
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.marti.proje.model.Place
import com.marti.proje.retrofit.MapApi
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class Repository(apiService:MapApi) {

    val api = apiService

    suspend fun getPlace(query:String?) = api.getPlaces(query)

}