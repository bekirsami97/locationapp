package com.marti.proje.install

import android.app.Application
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.dsl.module
import com.marti.proje.di.modules
import com.marti.proje.di.viewModelModules

class Application: Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@Application)
            modules(listOf(modules, viewModelModules))
        }
    }
}