package com.marti.proje.mapActivity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.marti.proje.R
import com.marti.proje.detailActivity.DetailActivity
import com.marti.proje.model.Place
import com.marti.proje.model.Result

class MapActivity : AppCompatActivity(),OnMapReadyCallback{
    lateinit var dataModel:Result
    lateinit var latLng:LatLng
    lateinit var title:String
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_map)

        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as? SupportMapFragment
        mapFragment?.getMapAsync(this)

         dataModel = intent.extras?.getSerializable("dataModel") as Result

         val lat = dataModel.geometry?.location?.lat
         val lon = dataModel.geometry?.location?.lng

         title = dataModel.formatted_address.toString()
         latLng = LatLng(lat!!,lon!!)

    }

    override fun onMapReady(googleMap: GoogleMap?) {
        googleMap?.apply {
            addMarker(
                MarkerOptions()
                    .position(latLng)
                    .title(title)
            )

            moveCamera(CameraUpdateFactory.newLatLng(latLng));
            animateCamera(CameraUpdateFactory.newLatLngZoom(latLng,14f));

            setOnMarkerClickListener {
                val intent = Intent(this@MapActivity,DetailActivity::class.java)
                intent.putExtra("dataModel",dataModel)
                startActivity(intent)
                return@setOnMarkerClickListener true
            }

        }
    }


}
