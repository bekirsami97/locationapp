package com.marti.proje.detailActivity

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.Glide
import com.marti.proje.R
import com.marti.proje.databinding.BindingDetailActivity
import com.marti.proje.install.SharedVariables
import com.marti.proje.model.Result
import com.squareup.picasso.Picasso
import org.koin.android.viewmodel.ext.android.viewModel


class DetailActivity : AppCompatActivity() {

    lateinit var dataModel:Result
    lateinit var mBind:BindingDetailActivity
    val mViewModel:DetailActivityViewModel by viewModel()
    val width:Int = 800
    val height:Int = 800
    lateinit var photoReference:String
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBind = DataBindingUtil.setContentView(this, R.layout.activity_detail)

        dataModel = intent?.extras?.getSerializable("dataModel") as Result
        photoReference = dataModel.photos?.get(0)?.photo_reference.toString()

        Picasso.with(this)
            .load(getPlacePhoto())
            .placeholder(R.drawable.placeholder)
            .into(mBind.imageViewDetail)

        Log.i("MyLogger",getPlacePhoto())

        mBind.textViewPlace.text = dataModel.formatted_address

    }

    fun getPlacePhoto():String{
        var url = "https://maps.googleapis.com/maps/api/place/photo?"
        val key ="key="+SharedVariables.API_KEY
        val photoreference = "photoreference=$photoReference"
        val maxWidth = "maxwidth=$width"
        val maxHeight = "maxheight=$height"
        url = "$url&$key&$photoreference&$maxWidth&$maxHeight"
        return url
    }

}
