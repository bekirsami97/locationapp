package com.marti.proje.retrofit

import com.marti.proje.install.SharedVariables
import com.marti.proje.model.Place
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query

interface MapApi {


@GET("api/place/textsearch/json")
suspend fun getPlaces(@Query("query") query:String?,
                      @Query("key") api_key:String = SharedVariables.API_KEY):Place

}