package com.marti.proje.di

import com.marti.proje.detailActivity.DetailActivityViewModel
import com.marti.proje.mainFragment.MainFragmentViewModel
import com.marti.proje.repository.Repository
import com.marti.proje.retrofit.AuthInterceptor
import com.marti.proje.retrofit.provideApi
import com.marti.proje.retrofit.provideOkHttpClient
import com.marti.proje.retrofit.provideRetrofit
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val modules = module {
    factory { AuthInterceptor() }
    factory { provideOkHttpClient(get()) }
    factory { provideApi(get()) }
    single { provideRetrofit(get()) }
    single { Repository(get()) }
}

val viewModelModules = module {
    viewModel{MainFragmentViewModel(get())}
    viewModel { DetailActivityViewModel(get()) }
}