package com.marti.proje.mainActivity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.marti.proje.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
